const { Client } = require('whatsapp-web.js');
const client = new Client({ puppeteer: {headless: true, args: ['--no-sandbox', '--disable-setuid-sandbox']} });
// You can use an existing session and avoid scanning a QR code by adding a "session" object to the client options.
// This object must include WABrowserId, WASecretBundle, WAToken1 and WAToken2.
const dbot = require('dbot-js');

exports.main = async function (req, res, next) {
    try {

        client.on('qr', (qr) => {
            // Generate and scan this code with your phone
            console.log('QR RECEIVED', qr);
        });

        client.on('ready', () => {
            console.log('Client is ready!');
        });

        client.on('message', msg => {
            if (msg.body) {
                let message = msg.body;
                console.log(`PESAN => ${message}`)
                dbot.get_response(message, function (err, result) {
                    if (!err) {
                        console.log(`JAWABAN => ${result}`)
                        msg.reply(result);
                    }
                })
            }
        });

        client.initialize();
    } catch (error) {
        console.log(error.message)
        next()
    }
}