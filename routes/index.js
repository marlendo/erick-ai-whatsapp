const express = require('express');
const router = express.Router();
const dbot = require('dbot-js');
const response = require('../helper');

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/test-bot/:id', function (req, res, next) {
  let message = req.params.id;
  dbot.get_response(message, function(err, result){
    if (!err) {
      response.success(res, result)
    }
 })
});

module.exports = router;
