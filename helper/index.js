const errorCode = require('../constant');

exports.failed = function (res, err, code) {
  console.log(err)  
  res.status(200).json({
    error: true,
    errorCode: code,
    message: errorCode.errorMessage(code),
    data: err
  });
}

exports.success = function (res, data) {
  res.status(200).json({
    error: false,
    errorCode: null,
    message: "success",
    data
  });
}